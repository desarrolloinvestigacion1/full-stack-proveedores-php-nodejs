const CONSTANTS = require('../config/constants');
const fs = require('fs');

class Provider {
  constructor(provider) {
    this._id = Date.now();
    this.nombre = provider.nombre;
    this.razon_social = provider.razon_social;
    this.direccion = provider.direccion;
    this.created_At = new Date();
    this.updated_At = new Date();
  }

  /**
   * Obtiene todos los datos del archivo JSON.   
   */
  static getAllData() {
    return new Promise((resolve, reject) => {
      fs.readFile(CONSTANTS.BD_JSON, 'utf8', (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(JSON.parse(data));
        }
      });
    });
  }

  /**
   * Guarda los datos en el archivo JSON.
   * @param {object} data - Datos a guardar en el archivo.   
   */
  static saveData(data) {
    return new Promise((resolve, reject) => {
      const jsonData = JSON.stringify(data);
      
      fs.writeFile(CONSTANTS.BD_JSON, jsonData, 'utf8', err => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  /**
   * Crea un nuevo proveedor.
   * @param {object} provider - Datos del proveedor a crear.
   * @param {function} result - Función de retorno de llamada.   
   */
  static create(provider, result) {
    let newProvider; // Mover la declaración de newProvider fuera del ámbito
  
    Provider.getAllData()
      .then(data => {
        newProvider = new Provider(provider); // Asignar el valor a newProvider
        newProvider._id = Date.now();
        data.providers.push(newProvider);
        return Provider.saveData(data);
      })
      .then(() => {
        result(null, newProvider._id); // Acceder a newProvider en el ámbito superior
      })
      .catch(err => {
        result(err, null);
      });
  }
  
  /**
   * Obtiene la lista de proveedores.
   * @param {function} result - Función de retorno de llamada.   
   */
  static read(result) {
    Provider.getAllData()
      .then(data => {
        result(null, data.providers);
      })
      .catch(err => {
        result(err, null);
      });
  }

   /**
   * Obtiene los datos de los proveedores para mostrar en una tabla paginada en datatable.
   * @param {object} req - Objeto de solicitud.
   * @param {function} result - Función de retorno de llamada.   
   */
  static readDataTable(req, result) {
    // Extrae los parámetros de la solicitud
    const draw = req.query.draw;
    const start = parseInt(req.query.start);
    const length = parseInt(req.query.length);
    const order_data = req.query.order;

    let column_name, column_sort_order;

    // Verifica si se proporciona el orden de clasificación
    if (typeof order_data === 'undefined') {
      column_name = '_id'; // Si no se proporciona, se establece el campo de orden por defecto en '_id'
      column_sort_order = 'asc'; // El orden por defecto es ascendente
    } else {
      const column_index = parseInt(req.query.order[0]['column']);
      column_name = req.query.columns[column_index]['data'];
      column_sort_order = req.query.order[0]['dir'];
    }

    const search_value = req.query.search['value'].toLowerCase(); // Convierte a minúsculas el valor de búsqueda

    // Obtiene todos los datos existentes
    Provider.getAllData()
      .then(data => {
        const providers = data.providers;

        let total_records = providers.length; // Obtiene el número total de registros

        // Filtra los proveedores que coinciden con el valor de búsqueda
        let filtered_providers = providers.filter(provider => {
          return (
            provider.nombre.toLowerCase().includes(search_value) || // Comprueba si el nombre del proveedor coincide
            provider.razon_social.toLowerCase().includes(search_value) // Comprueba si la razón social del proveedor coincide
          );
        });

        let total_records_with_filter = filtered_providers.length; // Obtiene el número total de registros después de aplicar el filtro

        filtered_providers.sort((a, b) => {
          // Ordena los proveedores filtrados según la columna de orden y el orden de clasificación
          const valueA = a[column_name] ? a[column_name].toString() : '';
          const valueB = b[column_name] ? b[column_name].toString() : '';

          if (column_sort_order === 'asc') {
            return valueA.localeCompare(valueB); // Compara los valores en orden ascendente
          } else {
            return valueB.localeCompare(valueA); // Compara los valores en orden descendente
          }
        });

        let sliced_providers = filtered_providers.slice(start, start + length); // Obtiene los proveedores paginados

        let data_arr = [];

        sliced_providers.forEach(row => {
          // Construye un arreglo de objetos con los datos de los proveedores para datatable.
          data_arr.push({
              '0': `<button class="btn btn-warning btn-xs" onclick="consultarActualizar(${row._id})"><i class="fa fa-pencil"></i></button>` +
                  ` <button class="btn btn-danger btn-xs" onclick="eliminar(${row._id})"><i class="fa fa-trash"></i></button>`,
              '1': row._id,
              '2': row.nombre,
              '3': row.razon_social,
              '4': row.direccion,
              '5': row.created_At,
              '6': row.updated_At,
            });
          });
     
             result(null, {
               draw: draw,
               recordsTotal: total_records,
               recordsFiltered: total_records_with_filter,
               data: data_arr
             });
           })
           .catch(err => {
             result(err, null);
           });
  }

  /**
   * Lee un proveedor por su ID.
   * @param {number} id - ID del proveedor a leer.
   * @param {function} result - Función de retorno de llamada.
   */
  static readById(id, result) {    
    Provider.getAllData()
      .then(data => {
        const provider = data.providers.find(p => parseInt(p._id) === parseInt(id));

        if (provider) {
          result(null, provider);
        } else {
          result({ message: 'Provider not found' }, null);
        }

      })
      .catch(err => {
        result(err, null);
      });
  }

/**
 * Lee proveedores por nombre.
 * @param {string} nombre - Nombre del proveedor.
 * @param {function} result - Función de retorno de llamada.
 */
static readByName(nombre, result) {
  // Leer el archivo bd.json
  fs.readFile(CONSTANTS.BD_JSON, 'utf8', (err, data) => {
    if (err) {
      result(err, null); // Llama a la función de retorno de llamada con un error si ocurre algún problema al leer el archivo
    } else {
      const jsonData = JSON.parse(data);
      const providers = jsonData.providers;

      // Filtrar proveedores por nombre
      const existingProvider = providers.find(provider =>
        provider.nombre.toLowerCase() === nombre.toString().toLowerCase()
      );

      result(null, existingProvider); // Llama a la función de retorno de llamada con el proveedor encontrado
    }
  });
}

 /**
   * Lee proveedores por nombre y/o razón social.
   * @param {number} page - Número de página para la paginación.
   * @param {number} limit - Número máximo de registros por página.
   * @param {string} nombre - Nombre del proveedor (opcional).
   * @param {string} razon_social - Razón social del proveedor (opcional).
   * @param {function} result - Función de retorno de llamada.   
   */
  static readByFullName(page, limit, nombre, razon_social, result) {
    const offset = (page - 1) * limit;

    // Leer el archivo bd.json
    fs.readFile(CONSTANTS.BD_JSON, 'utf8', (err, data) => {
      if (err) {
        result(err, null); // Llama a la función de retorno de llamada con un error si ocurre algún problema al leer el archivo
      } else {
        const jsonData = JSON.parse(data);
        const providers = jsonData.providers;

        // Aplica los filtros de búsqueda
        let filteredProviders = providers;

        if (nombre) { 
          filteredProviders = filteredProviders.filter(provider => provider.nombre.toLowerCase().includes(nombre.toLowerCase())); // Filtrar los proveedores cuyo campo 'nombre' contiene el valor especificado, ignorando las diferencias de mayúsculas y minúsculas.
        }

        if (razon_social) {
          filteredProviders = filteredProviders.filter(provider => provider.razon_social.toLowerCase().includes(razon_social.toLowerCase())); // Filtrar los proveedores cuyo campo 'razon_social' contiene el valor especificado, ignorando las diferencias de mayúsculas y minúsculas.
        }        

        // Aplicar la paginación
        const paginatedProviders = filteredProviders.slice(offset, offset + limit);

        result(null, paginatedProviders); // Llama a la función de retorno de llamada con los proveedores filtrados y paginados
      }
    });
  }


  /**
 * Actualiza un proveedor por su ID.
 * @param {number} id - ID del proveedor a actualizar.
 * @param {object} provider - Datos actualizados del proveedor.
 * @param {function} result - Función de retorno de llamada.
 */
  static async update(id, provider, result) {
    try {
      const data = await Provider.getAllData(); // Espera a que se resuelva la promesa y obtiene los datos
      const existingProvider = data.providers.find(p => parseInt(p._id) === parseInt(id)); // Busca el proveedor existente por su _id
      
      if (existingProvider) {
        // Actualiza los campos del proveedor existente con los nuevos valores
        const updatedProvider = {
          ...existingProvider,
          nombre: provider.nombre,
          razon_social: provider.razon_social,
          direccion: provider.direccion,
          updated_At: new Date()
        };

        const index = data.providers.indexOf(existingProvider);
        data.providers[index] = updatedProvider; // Reemplaza el proveedor existente con el proveedor actualizado
        await Provider.saveData(data); // Espera a que se guarden los datos actualizados en el archivo
        
        result(null, updatedProvider); // Llama a la función de retorno de llamada con el proveedor actualizado
      } else {
        result({ message: 'Provider not found' }, null); // Llama a la función de retorno de llamada con un mensaje de error si el proveedor no se encuentra
      }
    } catch (err) {
      result(err, null); // Llama a la función de retorno de llamada con el error
    }
  }

  /**
   * Elimina un proveedor por su ID.
   * @param {number} id - ID del proveedor a eliminar.
   * @returns {Promise<object>} - Promesa que resuelve con un objeto que contiene el mensaje de éxito.
   */
  static async delete(id, result) {
    try {
      const data = await Provider.getAllData();
      const index = data.providers.findIndex(p => parseInt(p._id) === parseInt(id));

      if (index !== -1) {
        data.providers.splice(index, 1);
        await Provider.saveData(data);
        
        result(null, { message: 'Provider deleted' })
      } else {
        result(null, { message: 'Provider not found' })      
      }
    } catch (error) {
      console.error('Error deleting provider:', error);
      throw error;
    }
  }

}
  
module.exports = Provider;
     