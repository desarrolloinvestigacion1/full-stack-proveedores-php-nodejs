const express = require("express");
const router = express.Router();
const InformationController = require("../controllers/informationController");

// Consultar mensaje de bienvenida.
router.get("/informationWelcome", InformationController.getWelcomeMessage);

// Consultar version.
router.get("/informationVersion", InformationController.getVersion);

module.exports = router;
