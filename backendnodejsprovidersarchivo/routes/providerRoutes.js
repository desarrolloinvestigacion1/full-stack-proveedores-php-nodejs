const express = require("express");
const router = express.Router();
const ProviderController = require("../controllers/providerController");

// Alta de proveedor.
router.post("/provider", ProviderController.createProvider);

// Consultar todos los proveedores.
router.get("/provider", ProviderController.readProvider);

// Consultar proveedor por Id.
router.get("/provider/:providerId", ProviderController.readProviderById);

// Consultar proveedores paginado con DataTable.
router.get("/providerDataTable", ProviderController.readDataTable);

// Actualizar Proveedores.
router.put("/provider/:providerId", ProviderController.updateProvider);

// Eliminar proveedores
router.delete("/provider/:providerId", ProviderController.deleteProvider);

module.exports = router;
