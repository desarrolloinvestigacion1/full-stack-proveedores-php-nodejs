const Provider = require("../models/providerModel");

/**
 * Clase para invocar las altas, bajas y cambios de proveedores en un archivo con formato JSON. 
 */
class ProviderController {
 
  /**
   * Crea un proveedor.
   */
  static async createProvider(req, res) {
    try {
      // Validar los campos requeridos
      const validationErrors = ProviderController.validateProviderFields(req.body);
      if (validationErrors.length > 0) {
        return res.status(422).json(validationErrors);
      }

      // Verificar si el proveedor ya existe un proveedor con el mismo nombre.
      await ProviderController.checkExistingProviderByName(req);    

      // Crear el proveedor
      const provider = new Provider(req.body);

      const createdProviderId = await new Promise((resolve, reject) => {
        Provider.create(provider, (err, createdProviderId) => {
          if (err) {
            reject(err);
          } else {
            resolve(createdProviderId);
          }
        });
      });

      const createdProvider = {
        id: createdProviderId,
        nombre: provider.nombre,
        razon_social: provider.razon_social,
        direccion: provider.direccion,        
      };

      res.json(createdProvider);
    } catch (err) {
      res.status(500).send(err.message);
    }
  }


  /**
   * Valida los campos del proveedor.   
   */
  static validateProviderFields(providerData) {
    const errors = [];

    if (!providerData.nombre) {
      errors.push({ nombre: "Nombre is required" });
    }

    if (!providerData.razon_social) {
      errors.push({ razon_social: "Razón Social is required" });
    }

    if (!providerData.direccion) {
      errors.push({ direccion: "Dirección is required" });
    }

    return errors;
  }

  /**
   * Verifica si ya existe un proveedor con el mismo nombre.   
   */
  static checkExistingProviderByName(req) {
    const nombre = req.body.nombre;

    return new Promise((resolve, reject) => {
      Provider.readByName(nombre, (err, existingProvider) => {
        
        if (err) {
          console.error(err);
          reject({ status: 500, message: "Error searching for provider" });
        } else if (existingProvider) {          
          reject({ status: 409, message: "A provider with the same name already exists" });
        } else {
          resolve({ status: 200, message: "The provider does not exist" });
        }
        
      });
    });
  }

  /**
   * Consulta todos los proveedores.   
   */
  static async readProvider(req, res) {    
    try {
      const providers = await new Promise((resolve, reject) => {
        Provider.read((err, providers) => {
          if (err) {
            reject(err);
          } else {
            resolve(providers);
          }
        });
      });

      res.json(providers);
    } catch (err) {
      res.status(500).send(err.message);
    }
  } 
  
  /**
   * Consulta de forma paginada los proveedores para ser utilizado por un datatable.    
   */
  static async readDataTable(req, res, next) {
    //console.log("Campos recibidos:", req.query);
    try {
      const providers = await new Promise((resolve, reject) => {
        Provider.readDataTable(req, (err, result) => { // Llamar a Provider.read y pasar el callback
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      });
  
      res.json(providers);
    } catch (err) {
      res.status(500).send(err.message);
    }
  }  

  /**
   * Consulta un proveedor por su Id.   
   */
  static async readProviderById(req, res) {
    try {
      const id = req.params.providerId;      

      Provider.readById(id, (err, provider) => {
        if (err) {
          console.log(res.status(500).send(err.message));
          return res.status(500).send(err.message);
        }

        res.json(provider);
      });
    } catch (err) {
      console.log(res.status(500).send(err.message));
      res.status(500).send(err.message);
    }
  } 

  /**
   * Actualiza la información de un proveedor.   
   */
  static async updateProvider(req, res) {
    try {
      const id = req.params.providerId;
      const provider = new Provider(req.body);

      // Validar los campos requeridos
      const validationErrors = ProviderController.validateProviderFields(req.body);
      if (validationErrors.length > 0) {
        return res.status(422).json(validationErrors);
      }
      
      // Verificar si el proveedor ya existe un proveedor con el mismo nombre.
      await ProviderController.checkExistingProviderByName(req);    

      Provider.update(id, provider, (err, updatedProvider) => {
        if (err) {
          return res.status(500).send(err.message);
        }

        res.json(updatedProvider);
      });
    } catch (err) {
      res.status(500).send(err.message);
    }
  }

  /**
   * Elimina a un proveedor por su Id.   
   */
  static async deleteProvider(req, res) {
    try {
      const id = req.params.providerId;

      Provider.delete(id, (err, deletedProvider) => {
        if (err) {
          return res.status(500).send(err.message);
        }

        res.json(deletedProvider);
      });
    } catch (err) {
      res.status(500).send(err.message);
    }
  }
}

module.exports = ProviderController;
