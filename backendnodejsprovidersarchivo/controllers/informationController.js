const CONSTANTS = require('../config/constants');

/**
 * Clase para obtener información del sistema.
 * Obtiene el mensaje de bienvenida y la versión del sistema.
 */
class InformationController {   

  static getWelcomeMessage(req, res) {
    const message = { greeting: CONSTANTS.GREETING };
    return res.json(message);
  }

  static getVersion(req, res) {
    const message = { version: CONSTANTS.VERSION };
    return res.json(message);
  }
}

module.exports = InformationController;
