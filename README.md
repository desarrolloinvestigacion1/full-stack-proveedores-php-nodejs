# FRONT Y BACKEND PARA PROVEEDORES

#### Proyecto con las siguientes características:



- #### NodeJS

- #### PHP

- #### Archivo en formato JSON (bd.json)
​    



## Introducción
### Agradecimientos  

Les agradezco mucho a todos la atención y el apoyo brindado para este proyecto. 



## Contexto
### Esta aplicación Full Stack realiza lo siguiente:

```
El objetivo de este proyecto, es realizar la alta, baja y cambios de proveedores, esto se realizará con un archivo bd.json en formato JSON para almacenar la información.

Para el Front utilizaremos Apache con lenguaje PHP, Html, JavaScript, Bootstrap, Datatables principalmente para mostrar información en Grid.

Se utilizará el software Laragon para fácil instalación y ambientación de PHP y Apache.

Para la parte del Backend, utilizaremos NodeJS.

El proyecto utiliza el patrón MVC para mayor claridad y fácil lectura.

En la sección de requerimientos se especificará más a detalle las características necesarias para el proyecto, tanto para el Front como para el Backend.



```


## Requerimientos Necesarios

**Sistema Front**
```
+ Nombre del sistema: frontprovidersbdjson

+ Requerimientos:
	- Apache httpd-2.4.54
	- PHP 8.1.10
	- bootstrap
	- JQuery
	- Material Icons
	- DataTables
	- AdminLTE
```



**Sistema Backend**
```
+ Nombre del sistema: backendnodejsprovidersarchivo

+ Requerimientos:
  - Node.js v18.12.1

```


## Instalaciones

En el documento MAN - Full Stack Proveedores.docx, viene un poco de ilustración para la instalaciones necesarias.

**Laragon - Instalarlo Para el Sistema Front con PHP**

Descargar y realizar los siguientes pasos para ejecutar el sistema front frontprovidersbdjson.

```
+ Descargar la versión full en la siguiente url:
https://laragon.org/download/

```


**NodeJS - Para el Sistema Backend**

+ Descargar e instalar nodejs de la siguiente url:

https://nodejs.org/en
​    
​    


### Sistema Front - Acceso y Configuración

**Una vez instalado Laragon, realizar lo siguiente:**

```
+ Abrir Laragon.

+ Click en el botón Iniciar Todo para que inicie Apache.

+ Click en el botón Root para entrar al directorio( laragon/www)  donde se pondrá el sistema Front.

+ Copiar el sistema frontproviderbdjson en el directorio laragon/wwww

+ Descargar el sistema front en está página: 

+ Para accesar al sistema, realizar lo siguiente:

+ Dar click en Menú en Laragon, se encuentra en la parte superior izquierda.

	+ También se puede dar click derecho en cualquier parte de la pantalla y salen las opciones.
	
+ Aparecerá un menú, dar click en www, y seleccionar el sistema frontproviderbdjson.

+ Con lo anterior se abrirá el sistema front

NOTA: Se puede utilizar también una instalación de Apache normal, XAMPP(https://www.apachefriends.org/) u otro software que tenga Apache.

```

### Sistema Backend - Acceso y Configuración

**Una vez instalado NodeJS, realizar lo siguiente:**

```
+ Descargar el sistema backend backendnodejsprovidersarchivo de está página en C:/backendnodejsprovidersarchivo de preferencia.

+ Abrir una consola de comando y Entrar al directorio c:/backendnodejsprovidersarchivo

+ Ejecutar el sistema con el comando npm start

```

## Postman - Documentación Backend

### Para ver los servicios del Backend y realizar pruebas a este mismo, abrir el siguiente archivo con Postman (https://www.postman.com/downloads/).

```
NodeJS Api Rest Providers Archivo.postman_collection.json
```


### Los llamados que contendrá el archivo de Postman son los siguientes:

#### **Consultar Bienvenida*

```
GET
http://localhost:3000/api/informationWelcome
```



#### Consultar Versión

```
GET
http://localhost:3000/api/informationVersion
```


#### Consultar Todos Los Proveedor

```
GET
http://localhost:3000/api/provider
```



#### Consultar Proveedor Por Id

```
GET
http://localhost:3000/api/provider/1687494022921
```



#### Actualizar Proveedor

```
PUT
http://localhost:3000/api/provider/1687494022921
```



#### Eliminar Proveedor

```
DELETE
http://localhost:3000/api/provider/1687477828873
```



#### Crear Proveedor

```
POST
http://localhost:3000/api/provider
```


## DATA
### Archibo bd.json
** Se utilizará  un archivo bd.json en formato JSON para almacenar la información en este proyecto.