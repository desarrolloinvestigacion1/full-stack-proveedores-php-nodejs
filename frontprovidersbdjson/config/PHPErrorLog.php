<?php
/**
* Clase para escribir logs desde php
*/
class LogLibrary {
    private $logdir = __DIR__;
    private $logname = false;  
    private $prefix = true;  
    private $separator = "\n";  
    private $_filehandler = false;  
	private $date_format = 'D d M Y H:m:s';

    public function log($str){
        if (!$this->logname){
            $this->logname = "log.log";
        }

        if (!$this->_filehandler) {
            $this->_createfile();
        }

        $this->_writeintofile($str);
    }

    private function _createfile(){
        $dir = explode(DIRECTORY_SEPARATOR.'private',$this->logdir);

        //$folder = $dir[0] .DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'LOG'.DIRECTORY_SEPARATOR;
        $folder = $dir[0] .DIRECTORY_SEPARATOR.'log'.DIRECTORY_SEPARATOR;
        $file = $folder.$this->logname;

        if (!is_dir($folder)) {
			mkdir($folder, 0777);		
		}
		
        $this->_filehandler = fopen($file,'a+');

        if (!$this->_filehandler) die("Error: no se pudo generar el fichero");
        return;
    }  

    private function _getprefix(){
        $prefix = '';
        $prefix .= "[".$this->_getIP() ." ".date("d-m-o H:i:s")."]";
        return $prefix;
    } 

    private function _writeintofile($str){
        if ($this->prefix) $str = $this->_getprefix()." ".$str;
        fwrite($this->_filehandler,$str.$this->separator);
    }
	
	private function _getIP(){
		return ($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 0;
	}

    function __destruct(){
        if (!$this->_filehandler) return;
        fclose($this->_filehandler);    //close the file handler
    }  

}
?>
