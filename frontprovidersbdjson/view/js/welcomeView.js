/**
 * Configuracion Inicial.
 */
function init(){	
    readWelcomeMessage();
    readVersion();
    
    $(".continue-button").click(function() {
        window.location.href = "providersView.php";
    });
}

/**
 * Consulta el mensaje de bienvenida.
 */
function readWelcomeMessage() {	
	var sParametros = {id : ""}; // Se pasan par&aacutemetros en formato JSON, si no no funciona para el dataType JSON.			
	
	$.ajax({
		url: "http://localhost:3000/api/informationWelcome",// Checar, modificar.		
		type: "GET",
		data: sParametros,				
		dataType: 'JSON',
		success: function(response){						
			$("#welcome-text").text(response.greeting);			
		},
		error: function(response)
		{            
			errorMessage(response);
		}
	});	
}

/**
 * Consulta la versión del sistema.
 */
function readVersion() {	
    var sParametros = {id : ""}; // Se pasan par&aacutemetros en formato JSON, si no no funciona para el dataType JSON.			
    
    $.ajax({
        url: "http://localhost:3000/api/informationVersion",// Checar, modificar.		
        type: "GET",
        data: sParametros,				
        dataType: 'JSON',
        success: function(response){						
            $("#version").text(response.version);			
        },
        error: function(response)
        {
            errorMessage(response);
        }
    });	
}	

/**
 * Despliega mensaje de error. 
 */
function errorMessage(response) {   
    //console.log(response);
    bootbox.alert(
        "Error: " + "Verificar comunicación" + "<br>" +  //Checar, es response.codRetorno.
        "C&oacutedigo: " + response.status + "<br>" +  //Checar, es response.codRetorno.
        "Mensaje: <br>" + response.responseText); //Checar, es response.mensaje.			
}

$(document).ready(function(){
init();
});