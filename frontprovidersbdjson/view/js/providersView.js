var tabla; // Variable para el manejo de datatable.

/**
 * Configuracion Inicial.
 */
function init(){	
	mostrarform(false);
   	limpiar();
   	consultarPaginado();	
}

function limpiar(){	
	// Checar, modificar.
	$("#id").val(""); // Id.
	$("#nombre").val("");	
	$("#razon_social").val("");	
	$("#direccion").val("");	
}

/**
 * Muestra u oculta el formulario de captura o actualización.
 */
function mostrarform(flag){	
	if(flag){
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		$("#btnActualizar").prop("disabled",false);
		$("#btnActualizar").hide();
		
		$("#id").focus();
	}else{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();		
		$("#btnActualizar").show();
	}
}

/**
 * Despliega el formulario de captura.
 */
function agregarForm(){	
	limpiar();
	mostrarform(true);	
	//Checar, modificar.
	//$("#id").prop("disabled", false);	
	$("#id").prop("disabled", true); // Checar, se inhibio.
	$("#id").focus();	
	$("#btnGuardar").show();
	$("#btnActualizar").hide();
}

/**
 * Oculta el formulario.
 */
function cancelarform(){	
	limpiar();
	mostrarform(false);
}

/**
 * Consulta la información y la despliega en el datatable.
 */
function consultarPaginado(){	
	tabla = $('#tbllistado').dataTable({
		// columnDefs: [{
        //     orderable: true,
        //     targets: [1,2,3]
        // }],
		columnDefs: [ // Se defininen las columnas del datatable.
			{
				name: '_id',
            	orderable: true,
            	targets: [1]
        	},
			{
				name: 'nombre',
            	orderable: true,
            	targets: [2]
        	},
			{
				name: 'razon_social',
            	orderable: true,
            	targets: [3]
        	},
			{
				name: 'direccion',
            	orderable: true,
            	targets: [4]
        	},
			{
				name: 'created_at',
            	orderable: true,
            	targets: [5]
        	},
			{
				name: 'updated_at',
            	orderable: true,
            	targets: [6]
        	},
		],
		"aProcessing": true,//activamos el procedimiento del datatable		
		"bServerSide": true,//paginacion y filrado realizados por el server
		"serverMethod": 'GET', //Checar, se inhibio.
		
		"ajax":
		{			
			url:'http://localhost:3000/api/providerDataTable',	// Checar, modificar.
			type: "GET",
			dataType : "json",
			dataFilter: function(data) {
                // Forzar la codificación en UTF-8
                return decodeURIComponent(encodeURIComponent(data));
            },			
			error:function(e){
				console.log(e.responseText);
				alert("Error AJAX: " + e.responseText);
			}
		},
		"language": {
            url: '//cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json'
        },
		// Checar, se agreg&oacute.
		"paging" : true,
        //"scrollY" : 400,
        "searching" : true,
        "ordering" :  true,		
		"bDestroy":true,
		"pageLength": 5,				
		"lengthMenu": [5, 10, 20, 50, 100, 200, 500],
		'bPaginate': true,		
		"order":[[1,"asc"]]//ordenar (columna, orden)
	}).DataTable();
}

/**
 * Guarda la información capturada en el formulario.
 */
function insertar(){	
	$("#btnGuardar").prop("disabled",true);

	//Checar, modificar.
	var sParametros = {		
		nombre: $("#nombre").val(),
		razon_social: $("#razon_social").val(),
		direccion: $("#direccion").val()		
	}; 
	
	var sParametros = JSON.stringify(sParametros);
	$.ajax({		
		url: "http://localhost:3000/api/provider/", //Checar, modificar.		
		type: "POST",		
		data: sParametros,		
	   	dataType: 'JSON',
		contentType: 'application/json',

		success: function(response){										
			bootbox.alert("<p><br>Transacci&oacuten Exitosa!!!.<p><br>" + 
			"Id: " + response.id + "<br>" + 
			"Nombre: " + response.nombre + "<br>" + 
			"Razon Social: " + response.razon_social + "<br>" + 
			"Dirección: " + response.direccion + "<br>"			
			);
			
			$("#btnGuardar").prop("disabled",false);			
			limpiar();			
			$("#id").focus();
			
			tabla.ajax.reload( function ( json ) { // Se refresca la tabla y se deja en la misma p&aacutegina.
				$('#tbllistado').val( json.lastInput );
			}, false );
			$("#id").focus();
		},
	   error: function(response)
	   {
			errorMessage(response);			
			$("#btnGuardar").prop("disabled",false);			
	   }
	});

}

/**
 * Consulta un proveedor por su Id.
 * Está función se invoca cuando se le da click al ícono de actualizar en el datatable.
 * Consulta con el id que se selecciono en el datatable y muestra la información en el formulario de Actualizar Datos. 
 */
function consultarActualizar(id){		
	$("#btnGuardar").prop("disabled",true);
	$("#btnGuardar").hide();
	
	var sParametros = {id : id}; // Se pasan par&aacutemetros en formato JSON, si no no funciona para el dataType JSON.				
	$.ajax({
		url: "http://localhost:3000/api/provider/" + id,// Checar, modificar.		
		type: "GET",
		data: sParametros,				
		dataType: 'JSON',

		success: function(response){			
			mostrarform(true);
			
			// Checar, modificar.
			$("#id").val(response._id);
			$("#nombre").val(response.nombre);
			$("#razon_social").val(response.razon_social);
			$("#direccion").val(response.direccion);			

			$("#id").prop("disabled",true);
			$("#btnActualizar").show();
		},
		error: function(response)
		{
			errorMessage(response);
			mostrarform(false);
			tabla.ajax.reload();
		}
	});	
}

/**
 * Actualiza la información capturada.
 */
function actualizar(){		
	var sParametros = {
		id : $("#id").val(), 
		nombre: $("#nombre").val(),		
		razon_social: $("#razon_social").val(),
		direccion: $("#direccion").val()
	}; 

	var sParametros = JSON.stringify(sParametros);

	$.ajax({		
		url: "http://localhost:3000/api/provider/" + $("#id").val(),				
		type: "PUT", //Checar, modificar.
		data: sParametros,		
	   	dataType: 'JSON',
		contentType: 'application/json',

	   success: function(response){		   			
			bootbox.alert("<p><br>Transacci&oacuten Exitosa!!!.<p>");
			
			limpiar();
			mostrarform(false);
			tabla.ajax.reload( function ( json ) { // Se refresca la tabla y se deja en la misma p&aacutegina.
				$('#tbllistado').val( json.lastInput );
			}, false );					
		},
	   error: function(response)
	   {		   
			errorMessage(response);
			mostrarform(false);			
			tabla.ajax.reload();
	   }
	});	
}

/**
 * Elimina la información por el Id. 
 */
function eliminar(id){
	bootbox.confirm("¿Esta seguro que desea eliminar este registro?", function(result){
		if (result) {
			$("#btnGuardar").prop("disabled",true);			
			var sParametros = {id : id}; // Se pasan par&aacutemetros en formato JSON, si no no funciona para el dataType JSON.			

			$.ajax({				
				url: "http://localhost:3000/api/provider/" + id, //Checar, eliminar.
				type: "DELETE", // Checar, modificar.
				data: sParametros,				
				dataType: 'JSON',

				success: function(response){					
					bootbox.alert("<p><br>Transacci&oacuten Exitosa!!!.<p>");
					
					mostrarform(false);					
					tabla.ajax.reload( function ( json ) { // Se refresca la tabla y se deja en la misma p&aacutegina.
						$('#tbllistado').val( json.lastInput );
					}, false );
				},
				error: function(response)
				{
					errorMessage(response);
					mostrarform(false);			
					tabla.ajax.reload();
				}
			});

			limpiar();
		}
	})
}

/**
 * Despliega mensaje de error con un formato. 
 */
function errorMessage(response) {       	
	const message = response.responseText.replace(/["{}\[\]]/g, '').replace(/,/g, ', ');	
    bootbox.alert(		 
		"C&oacutedigo: " + response.status + "<br>" +  //Checar, es response.codRetorno.
        "Mensaje: <br>" + message); //Checar, es response.mensaje.			
}

$(document).ready(function(){
init();
});