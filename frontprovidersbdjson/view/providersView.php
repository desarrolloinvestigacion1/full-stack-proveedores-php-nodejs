<?php 
//activamos almacenamiento en el buffer
ob_start();
session_start();
{
require 'headerView.php';
?>
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">

      <!-- Default box -->
      <div class="row">
        <div class="col-md-12">
      <div class="box">
<div class="box-header with-border">
  <h1 class="box-title">Cat&aacutelogo de Proveedores <button class="btn btn-success" onclick="agregarForm()" id="btnagregar"><i class="fa fa-plus-circle"></i>Agregar</button> <!--<a target="_blank" href="../reportes/rptarticulos.php"><button class="btn btn-info">Reporte</button></a>--></h1>
  <div class="box-tools pull-right">
    
  </div>
</div>
<!--box-header-->
<!-- DATATABLES INICIO -->
<!--centro-->
<div class="panel-body table-responsive" id="listadoregistros">
  <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
    <thead>
      <!--// Chedar, modificar.-->
      <th>Opciones</th>
      <th>_id</th>
      <th>nombre</th>      
      <th>razon_social</th>      
      <th>direccion</th>
      <th>created_at</th>
      <th>updated_at</th>      
    </thead>
    <tbody>
    </tbody>
    <tfoot>
      <!--// Chedar, modificar.-->
      <th>Opciones</th>
      <th>_id</th>
      <th>nombre</th>      
      <th>razon_social</th>      
      <th>direccion</th>
      <th>created_at</th>
      <th>updated_at</th>          
    </tfoot>   
  </table>
</div>
<!-- DATATABLES FIN -->
<!-- FORMULARIO INICIO -->
<div class="panel-body" id="formularioregistros">
  <!--<form action="" name="formulario" id="formulario" method="POST">-->
    <div class="form-group col-lg-6 col-md-6 col-xs-12">
      <label for="">Id Usuario(*):</label>
      <!--<input class="form-control" type="hidden" name="id" id="id">-->
      <input class="form-control" type="text" name="id" id="id" maxlength="100" placeholder="Id" required> <!--Checar-->
    </div>
    <div class="form-group col-lg-6 col-md-6 col-xs-12">
      <label for="">Nombre</label>
      <input class="form-control" type="text" name="nombre" id="nombre"  required>
    </div>    
    <div class="form-group col-lg-6 col-md-6 col-xs-12">
      <label for="">Raz&oacuten Social</label>
      <input class="form-control" type="text" name="razon_social" id="razon_social"  required>
    </div>    
    <div class="form-group col-lg-6 col-md-6 col-xs-12">
      <label for="">Direcci&oacuten</label>
      <input class="form-control" type="text" name="direccion" id="direccion"  required>
    </div>           
    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">      
      <button class="btn btn-primary" onclick="insertar()" id="btnGuardar"><i class="fa fa-save"></i>  Guardar</button>
      <button class="btn btn-primary" onclick="actualizar()" id="btnActualizar"><i class="fa fa-save"></i>  Actualizar</button>
      <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
    </div>
  <!--</form>-->
</div>
<!-- FORMULARIO FIN -->
<!--fin centro-->
      </div>
      </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<?php 
  require 'footerView.php'
?>
 <script src="../public/js/JsBarcode.all.min.js"></script>
 <script src="../public/js/jquery.PrintArea.js"></script> 
 <script src="js/providersView.js"></script>

 <?php 
}
?>