<!DOCTYPE html>
<html>
<head>
  <title>e-Commerce Gapsi</title>  
  <link rel="icon" href="../public/images/logo.ico" type="image/x-icon">
  <link rel="stylesheet" href="../public/css/welcome/welcome.css">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../public/css/bootstrap.min.css">
  <!-- Material Icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!-- JQuery -->
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="../public/js/bootstrap.min.js"></script>
  <script src="../public/js/bootbox.min.js"></script>  
  <script src="js/welcomeView.js"></script>  
</head>
<body>
  <div class="container">
    <div class="top-rectangle">
      <h1 class="title">e-Commerce GAPSI</h1>
      <div class="icon-circle">
        <i class="material-icons">more_vert</i>
      </div>
    </div>
    
    <div class="image-container">
      <img src="../public/images/logo.png" alt="Imagen" class="profile-image">
      <p id="welcome-text" class="welcome-text"></p>      
    </div>
    <button class="continue-button">Continuar</button>
      <p id="version" class="version"></p>
  </div>
</body>
</html>

<?php 
//require 'footerView.php'
?>